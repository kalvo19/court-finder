// Require the express package so that the framwework can be used
const express = require('express');

/*
 * Morgan package allows to see what requests have been asked by the browser. A log
 * of all the stuff requested by the user through the browser and the status of
 * each element requested (404, 200, 403, 201...).
 */
const morgan = require('morgan');

/*
 * Mongoose package allows to connect to a mongoDB database;
 */
const mongoose = require('mongoose');

// Compraise the express method within the Express class
const app = express();

const path = require('path');

mongoose.connect('mongodb://localhost:27017/court-finder', { useNewUrlParser: true })
    .then(db => console.log('Database is connected'))
    .catch(err => console.error(err));

/********** SETTINGS ***********/

/*
 * Set the port as the provided by default from the operating system => process.env.PORT.
 * Otherwise set a particular port for the web application.
 */
app.set('port', process.env.PORT || 3000);

/********** MIDDLEWARES ***********/
app.use(morgan('dev'));

/*
 * Each time the server gets information in a JSON format, as for instance in a form,
 * the server is going to be able to understand this information and it will treat them
 * as a JavaScript object so that the information can be processed faster. For example as to
 * store into a database.
 */
app.use(express.json());

/********** ROUTES ***********/
app.use('/api/courts', require('./routes/courts'));

/********** STATIC FILES ***********/

/*
 * Set the '/public' folder as a non-dynamical directory, where pictures, stylesheets, scripts and
 * assets are going to be stored.
 *
 * NOTA: path.join() => It is concatenating the current public where 'index.js' is located
 * plus 'public' so that the server could get access to the folder.
 */
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'assets')));

// Starts the web server of the application
app.listen(app.get('port'), () => {
    console.log(`Server on port ${app.get('port')}`);
});