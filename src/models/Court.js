/*
 * Requires only the class 'Schema' whereby it allows the creation of the models.
 */
const mongoose = require('mongoose');
const { Schema } = mongoose;

/*
 * Create the 'Schema' of our model (the structure with his properties and functions)
 */
const Court = new Schema({
    id: Number,
    name: String,
    aka: String,
    city: String,
    province: String,
    district: String,
    county: String,
    description: String,
    latitude: Number,
    longitude: Number,
    rate: Number,
    pictures: []
});

/*
 * Converts the Schema has been created to a database model and lets him to be imported
 * in other parts of the application.
 */
module.exports = mongoose.model('Court', Court);