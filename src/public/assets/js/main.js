let slider = {
    currentSlide: 0,
    carrousel: document.querySelector('.main_slider'),
    pictureHolder: document.querySelector('.slider_window'),
    pictures: document.getElementsByClassName('slider_picture'),
    controllers: document.getElementsByClassName('slider_controllers'),
    init: (event) => {
        console.log(slider.pictures);
        if (event.type !== 'resize') {
            slider.setup();
        }

        if (event.type === 'resize') {
            let distance = slider.pictures[0].offsetWidth;
            slider.update(distance);
        }

        slider.setSize(slider.pictureHolder, 'height', slider.pictures[0].offsetHeight);
    },
    setup: function() {
        $(slider.controllers[0]).click(function(event) {
            event.preventDefault();
            let distance = slider.pictures[0].offsetWidth;
            slider.moveBack(distance);
        });
        $(slider.controllers[1]).click(function(event) {
            event.preventDefault();
            let distance = slider.pictures[0].offsetWidth;
            slider.moveForward(-distance);
        });
    },
    update: function(distance) {
        let value = `translateX(${-distance * slider.currentSlide}px)`;
        console.log(value);
        slider.setProperty(slider.carrousel, 'transform', value);
    },
    moveBack: function(distance) {
        if (slider.currentSlide > 0) {
            slider.currentSlide--;
            let value = `translateX(${-distance * slider.currentSlide}px)`;
            slider.setProperty(slider.carrousel, 'transform', value);
        }
    },
    moveForward: function(distance) {
        if (slider.currentSlide < slider.pictures.length-1) {
            slider.currentSlide++;
            let value = `translateX(${distance * slider.currentSlide}px)`;
            slider.setProperty(slider.carrousel, 'transform', value);
        }
    },
    setProperty: function(element, property, value) {
        element.style[property] = value;
    },
    getProperty: function(element, property) {
        return element.style.getPropertyValue(property);
    },
    setSize: (element, type, value) => {
        element.style[type] = `${value}px`;
    }
};

$(document).ready(slider.init);
$(window).resize(slider.init);
