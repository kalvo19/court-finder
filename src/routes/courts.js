/*
 * App.js => This file includes all the routes with which the client is going
 *           to interact during its navigation.
 */

// Require the express package so that the framwework can be used
const express = require('express');

/*
 * Compraise the 'Router()' method within the express package. This method allow the
 * application to manage the routes that are being set into this file.
 */
const router = express.Router();

/********** COURTS ***********/

// Load the court model made by Mongoose
const Court = require('../models/Court');

router.get('/', async (req, res) => {

    // Gets all the courts stored
    let courts = await Court.find();
    res.json(courts);
});

router.post('/', async (req, res) => {

    // Creates a new court and saves in the mongoDB database
    const court = new Court(req.body);
    await court.save();
    res.json({
        status: 'Entry stored'
    });
});

router.put('/:id', async (req, res) => {

    /*
     * Updates the data that matches the court with the id provided in the URL by using
     * the PUT method.
     */
    const court = req.params.id;
    await Court.findByIdAndUpdate(req.params.id, req.body);
    res.json({
        status: 'Entry updated'
    });
});

router.delete('/:id', async (req, res) => {
    await Court.findByIdAndRemove(req.params.id);
    res.json({
        status: 'Entry deleted'
    });
});

/*
 * Allow to use 'express.Router()' method outside this file and all the routes this
 * file contains.
 */
module.exports = router;
